import pandas as pd
import numpy as np
from nltk.corpus import stopwords
from nltk.classify.scikitlearn import SklearnClassifier
from nltk import PorterStemmer, FreqDist
from nltk.tokenize import word_tokenize
from nltk.classify import accuracy as nltk_accuracy
from sklearn.ensemble import VotingClassifier
from sklearn.preprocessing import LabelEncoder
from sklearn import model_selection
from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression, SGDClassifier
from sklearn.naive_bayes import MultinomialNB
from sklearn.svm import SVC
from sklearn.metrics import confusion_matrix


number_of_features_to_use = 3000

df = pd.read_table('data/SMSSpamCollection', header=None, encoding='utf-8')
classes = df[0]

# preprocess
# convert classes ham=0, spam=1
encoder = LabelEncoder()
y = encoder.fit_transform(classes)

text_messages = df[1]

# use regular expressions to replace email addresses, URLs, phone numbers, other numbers
processed = text_messages.str.replace(r'^.+@[^\.].*\.[a-z]{2,}$', 'emailaddress')
processed = processed.str.replace(r'^http\://[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(/\S*)?$', 'webaddress')
processed = processed.str.replace(r'£|\$', 'moneysymb')
processed = processed.str.replace(r'^\(?[\d]{3}\)?[\s-]?[\d]{3}[\s-]?[\d]{4}$', 'phonenumbr')
processed = processed.str.replace(r'\d+(\.\d+)?', 'numbr')
processed = processed.str.replace(r'[^\w\d\s]', ' ')  # Remove punctuation
processed = processed.str.replace(r'\s+', ' ')  # Replace whitespace between terms with a single space
processed = processed.str.replace(r'^\s+|\s+?$', '')  # Remove leading and trailing whitespace
processed = processed.str.lower()

# remove step_words
step_words = set(stopwords.words('english'))
processed = processed.apply(lambda x: ' '.join(term for term in x.split() if term not in step_words))

# remove stemmers
ps = PorterStemmer()
processed = processed.apply(lambda x: ' '.join(ps.stem(term) for term in x.split()))

all_words = []
for msg in processed:
    words = word_tokenize(msg)
    for w in words:
        all_words.append(w)

all_words = FreqDist(all_words)

# print('number of words: {}'.format(len(all_words)))
# print('most common words: {}'.format(all_words.most_common(15)))

word_features = list(all_words.keys())[:number_of_features_to_use]


def find_features(msg):
    # define find features
    words = word_tokenize(msg)
    features = {}
    for word in word_features:
        features[word] = (word in words)

    return features


features = find_features(processed[0])

messages = list(zip(processed, y))
seed = 1
np.random.seed = seed
np.random.shuffle(messages)

feature_sets = [(find_features(text), label) for text, label in messages]
training, testing = model_selection.train_test_split(feature_sets, test_size=0.25, random_state=seed)

# print(len(training))
# print(len(testing))

names = ['KNeighbors', 'DecisionTree', 'RandomForest', 'LogisticRegression', 'SGD', 'naive_bayes', 'svm']

classifiers = [KNeighborsClassifier(), DecisionTreeClassifier(), RandomForestClassifier(), LogisticRegression(),
               SGDClassifier(max_iter=100), MultinomialNB(), SVC(kernel='linear')]

models = list(zip(names, classifiers))

# warp model in nltk
for name, model in models:
    nltk_model = SklearnClassifier(model)
    nltk_model.train(training)
    accuracy = nltk_accuracy(nltk_model, testing) * 100.0
    print('{}: Accuracy {}'.format(name, accuracy))


# for this stage i removed KNeighbors duo to poor results
nltk_ensemble = SklearnClassifier(VotingClassifier(estimators=models[1:], voting='hard', n_jobs=-1))
nltk_ensemble.train(training)
accuracy = nltk_accuracy(nltk_ensemble, testing) * 100
print('ensemble: Accuracy {}'.format(accuracy))


txt_features, labels = zip(*testing)

prediction = nltk_ensemble.classify_many(txt_features)
print(pd.DataFrame(confusion_matrix(labels, prediction), index=[['actual', 'actual'], ['ham', 'spam']],
                   columns=[['predicted', 'predicted'], ['ham', 'spam']]))
