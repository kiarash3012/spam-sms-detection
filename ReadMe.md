# This app tests 7 ai models  to classify spam sms from [this dataset](https://archive.ics.uci.edu/ml/datasets/SMS+Spam+Collection)
### Models: 1.KNeighbors, 2.DecisionTree, 3.RandomForest, 4.LogisticRegression, 5.SGD, 6.NaiveBayes, 7.SVM

---
## Classification Report:
    KNeighbors: Accuracy 94.04163675520459
    DecisionTree: Accuracy 97.91816223977028
    RandomForest: Accuracy 98.27709978463747
    LogisticRegression: Accuracy 98.34888729361091
    SGD: Accuracy 98.20531227566404
    naive_bayes: Accuracy 97.77458722182341
    svm: Accuracy 98.49246231155779
    
    ensemble: Accuracy 98.34888729361091
    
    confusion matrix:
                predicted     
                      ham spam
    actual ham       1202    0
           spam        23  168
---
##### python version:3.6

